#!/usr/bin/env sh

# Influx setup
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE telegraf"
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE sensor_data"
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE system_events"
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE user_actions"
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE faker_data"
curl -i -XPOST http://$INFLUX_HOST:8086/query --data-urlencode "q=CREATE DATABASE integration_data"
curl -i -XPOST "http://$INFLUX_HOST:8086/write?db=system_events" --data-binary "ran_seed value=1" || true

# Mysql setup
mysql -happdb -uroot -pgrownetics -e 'create database if not exists grownetics_test; grant all privileges on grownetics_test.* to grownetics;';
/var/www/html/bin/cake migrations migrate
/var/www/html/bin/cake migrations seed
/var/www/html/bin/cake migrations seed --source AclSeeds
