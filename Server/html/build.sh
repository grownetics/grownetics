#!/usr/bin/env bash

cd /var/www/html

mkdir tmp webroot/cache_js webroot/cache_css || true
mkdir -p tmp/cache tmp/sessions
chmod -R 777 tmp/ /tmp /var/www/html/webroot/uploads

# Install NPM
curl -fsSL https://deb.nodesource.com/setup_19.x | bash - &&\
apt-get install -y nodejs

# Install yarn
npm install -g yarn

yarn install
bin/cake asset_compress build
bin/cake cache clear_all

bin/cake asset_compress build

BUILD_DATE=`date +%Y-%m-%d:%H:%M:%S` && echo "<?php return ['BUILD_ID' => '$CI_PIPELINE_ID','BUILD_DATE' => '$BUILD_DATE'];" > config/build_info.php
