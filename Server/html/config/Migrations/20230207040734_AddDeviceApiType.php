<?php
use Migrations\AbstractMigration;

class AddDeviceApiType extends AbstractMigration
{

    public function up()
    {

        $this->table('devices')
            ->addColumn('api_type_id', 'integer', [
                'after' => 'owner_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('devices')
            ->removeColumn('api_type_id')
            ->update();
    }
}

