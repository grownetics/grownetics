<?php
/**
 * @var \App\View\AppView $this
 */
	use Cake\Core\Configure;
	$colors = Configure::read('Colors');

	$this->Html->scriptStart(['block' => 'scriptBottom']);
	echo '
		var GrowServer = GrowServer || {};
		(function() {
			GrowServer.Color = {
				getColorForMapItemType: function( map_item_type) {
					switch (map_item_type) {';
	foreach ($colors['map_item_types'] as $map_item_type => $color) {
		echo 'case "'.$map_item_type.'": return "'.$color.'";';
	}
	echo '			}
				},
				getColorForDataTypeAndValue: function( data_type, value) {
				    console.log("data_type:",data_type," value:",value);
					switch (data_type) {';
	foreach ($colors['data_type_values'] as $data_type => $values) {
		echo 'case "'.$data_type.'":';
			foreach($values as $value => $color) {
				echo 'if (value > '.$value.') { return "'.$color.'";}';
			}
            echo 'return "'.$color.'";';
		echo '    break;';
	}
	echo '
					}

				},
			};
		})();
	';
	$this->Html->scriptEnd();
?>
