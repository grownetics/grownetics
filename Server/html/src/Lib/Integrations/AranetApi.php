<?php

namespace App\Lib\Integrations;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use App\Lib\SystemEventRecorder;
use InfluxDB\Point;
use App\Lib\TimeSeriesWrapper;
use App\Lib\MessageQueueWrapper;

class AranetApi
{
  # Mapping of Aranet 'metric' to our internal Sensor Types
  # Aranet on the left, Grownetics on the right
  public $aranetSensorTypes = [
    1 => 57,
    2 => 61,
    3 => 62,
    4 => 63,
    7 => 66,
    8 => 64,
    9 => 65,
    10 => 58,
    11 => 59,
    12 => 60,
  ];

  # This function is called by GrowpulseShell, it pulls the most recent data, and processes it.
  public function poll($shell)
  {
    print_r("Begin poll");
    $this->Devices = TableRegistry::get("Devices");
    $this->Sensors = TableRegistry::get("Sensors");

    $messageQueueWrapper = new MessageQueueWrapper();
    $messages = [];
    $points = [];

    $devices = $this->Devices->findByApiTypeId($this->Devices->enumValueToKey('api_type', 'Aranet'));
    $ii = 0;
    foreach ($devices as $device) {
      print_r("\n\nGet data for device_id: ".$device->id ." api_id: ". $device->api_id."\n");
      if (!$device->api_id) {
        print_r("No device API ID, skip device: " . $device->id);
        continue;
      }
      $dataPoints = $this->latest($device->api_id);
      if (!isset($dataPoints->readings)) {
        continue;
      }
      foreach ($dataPoints->readings as $dataPoint) {
        if ($dataPoint->sensor == $device->api_id) {
          print_r("\n\nDATAPOINT: ");
          print_r($dataPoint);
          $this->Devices->updateDeviceInfo($device, ['id' => $device->id]);
          # A perfect match *chefs kiss*
          print_r("\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nProcessing data point");
          print_r($dataPoint);
          if (!isset($this->aranetSensorTypes[$dataPoint->metric])) {
            print_r("Unknown sensor type: " . $dataPoint->metric);
            continue;
          }
          $sensorTypeId = $this->aranetSensorTypes[$dataPoint->metric];

          $sensor = $this->getSensorForDataPoint($dataPoint, $device, $shell, $sensorTypeId);
          if (!$sensor) {
            $shell->out("No sensor found!!");
            continue;
          }

          $dataType = $this->Sensors->enumKeyToValue('sensor_data_type', $sensorTypeId);
          print_r("sensorTypeId: " . $sensorTypeId . " DataType: " . $dataType);
          $calibratedValue = $this->Devices->calibrate($sensor, $dataPoint->value);

          $point = new Point(
            'aranet', // name of the measurement
            (float) $dataPoint->value, // the measurement value
            [
              'source_type' => 0,
              'sensor_type' => $sensorTypeId,
              'data_type' => $dataType,
              'facility_id' => env('FACILITY_ID'),
              'source_id' => $sensor->id,
              'device_id' => $device->id,

            ],
            [], // optional additional fields
            strtotime($dataPoint->time)
          );
          print_r($point);
          $points[] = $point;
          $json = json_encode(array(array(
            'value' => (float) $calibratedValue,
            'source_id' => $sensor->id,
            'source_type' => 0,
            'data_type' => $dataType,
            'sensor_type' => $sensorTypeId,
            'device_id' => $device->id,
            'created' => $dataPoint->time,
            'facility_id' => (float) env('FACILITY_ID')
          )));
          array_push($messages, $json);
          Cache::write('sensor-value-' . $sensor->id, (float) $calibratedValue);
          Cache::write('sensor-time-' . $sensor->id, date("Y-m-d H:i:s"));
        } // if sensor matches the data point
      } // foreach datapoints

      if ($ii > 50) {
        # Attempt to save the DataPoint to the local Time Series DB
        $tsw = new TimeSeriesWrapper();
        $tsw->store($points, 'integration_data');

        print_r("Pushing: ");
        print_r($messages);
        $messageQueueWrapper->send($messages, 'data.sensor');

        $ii = -1;
        $messages = [];
        $points = [];
      }
      $ii++;
      print_r("II: " . $ii);
    } // foreach devices
  }

  public function getSensorForDataPoint($dataPoint, $device, $shell, $sensorTypeId)
  {
    $this->MapItems = TableRegistry::get("map_items");
    # Look up sensor type ID
    print_r("Senor type" . $sensorTypeId);
    # Get Sensor for sensor type
    $sensor = $this->Sensors->find('all', [
      'conditions' => [
        'device_id' => $device->id,
        'sensor_type_id' => $sensorTypeId
      ]
    ])->first();
    if (!$sensor) {
      $shell->out("No sensor, create one");
      $sensor = $this->Sensors->newEntity();
      $sensor->device_id = $device->id;
      $sensor->data_type = $this->Sensors->enumKeyToValue('sensor_data_type', $sensorTypeId);
      $sensor->sensor_type_id = $sensorTypeId;
      $sensor->label = 'Device ' . $device->id . ' - ' . $this->Sensors->enumKeyToValue('sensor_type', $sensorTypeId);
      $sensor->status = 1;
      $sensor->map_item_id = $device->map_item_id;
      $sensor->floorplan_id = 1;
      $sensor->dontMap = true;
      $this->Sensors->save($sensor);
      $shell->out($sensor);
    } else {
      $shell->out("Found sensor.");
    }
    return $sensor;
  }


  public function query($start, $end)
  {
    return $this->callApi("/data/?start_time=" . urlencode($start->format("Y-m-d H:i:s")) . "&end_time=" . urlencode($end->format("Y-m-d H:i:s")) . "&return_type=json");
  }

  public function latest($device_api_id)
  {
    return $this->callApi("/api/v1/measurements/history?seconds=300&sensor=" . $device_api_id);
    // return $this->callApi("/api/v1/measurements/last?sensor=" . $device_api_id);
  }

  public function callApi($url)
  {
    print_r("CALL API!!!!!\n\n\n");
    # Query Aranet
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://aranet.cloud" . $url,
      CURLOPT_HTTPHEADER => array(
        "ApiKey: " . env('ARANET_API_KEY'),
        "accept: application/json"
      ),
      CURLOPT_RETURNTRANSFER => true
    ));
    print_r($url);
    print_r($curl);
    # Check that we got a valid response, and decode it.
    $response = curl_exec($curl);
    print_r($response);
    $err = curl_error($curl);
    print_r($err);
    curl_close($curl);
    $jsonResponse = json_decode($response);
    // print_r($jsonResponse);
    return $jsonResponse;
  }

  # Store many points to the Timeseries Database.
  public function processBulkData($data, $shell)
  {
    $this->Devices = TableRegistry::get("Devices");
    $this->Sensors = TableRegistry::get("Sensors");

    # Store Data in InfluxDB
    $points = [];
    foreach ($data as $dataPoint) {
      $api_id = $dataPoint[0];
      $device = $this->Devices->findByApiId($api_id)->first();

      if (!$device) {
        continue;
      }
      $shell->out("Returned api_id: " . $device->api_id); //die();
      $shell->out("174 Found device " . $device->id . " for api_id " . $dataPoint[0]);
      $sensor = $this->getSensorForDataPoint($dataPoint, $device, $shell);
      $sensorTypeId = $this->infisenseSensorTypes[$dataPoint[1]];
      $dataType = $this->Sensors->enumKeyToValue('sensor_data_type', $sensorTypeId);

      $points[] = new Point(
        'aranet', // name of the measurement
        (float) $dataPoint[4], // the measurement value
        [
          'source_type' => 0,
          'sensor_type' => $this->infisenseSensorTypes[$dataPoint[1]],
          'data_type' => $this->Sensors->enumKeyToValue('sensor_data_type', $this->infisenseSensorTypes[$dataPoint[1]]),
          'facility_id' => env('FACILITY_ID'),
          'source_id' => $sensor->id,
          'device_id' => $device->id,

        ],
        [], // optional additional fields
        strtotime($dataPoint[3])
      );
    }

    # Attempt to save the DataPoint to the local Time Series DB
    $tsw = new TimeSeriesWrapper();
    $tsw->store($points, 'integration_data');
  }
}
