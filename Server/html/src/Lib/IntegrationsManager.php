<?php

namespace App\Lib;

use App\Lib\Integrations\InfisenseApi;
use App\Lib\Integrations\AranetApi;

class IntegrationsManager
{

  # Poll all our integrations that we need to pull data from.
  public function poll($shell)
  {
    if (env('INFISENSE_API_KEY',false)) {
      $infisenseApi = new InfisenseApi();
      $infisenseApi->poll($shell);
    }
    if (env('ARANET_API_KEY',false)) {
      $aranetApi = new AranetApi();
      $aranetApi->poll($shell);
    }
  }
}
