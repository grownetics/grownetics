.PHONY: web

serve:
	cd Server && docker-compose -f docker-compose.dashonly.yml up

env:
	cp Server/dev.env Server/.env

seed:
	docker exec -it server-growdash-1 ./seed.sh

web:
	cd Website && hugo -d ../site

web_serve:
	cd Website && hugo serve